# deezer-black

A super simple theme for Deezer that changes the colour scheme from indigo to pure black.

**Note that this theme will break deezers light theme**

[Install theme](https://userstyles.world/api/style/6658.user.css)

## Images

![playlists example](images/playlist.png)
![artists example](images/artist.png)
![charts example](images/chart.png)
